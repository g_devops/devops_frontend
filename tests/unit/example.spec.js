import AppRubleInput from "@/components/AppRubleInput";
import {mount} from "@vue/test-utils";
import Vuetify from "vuetify";
import Vue from "vue";

// Инициализация Vuetify
Vue.use(Vuetify);

describe('AppRubleInput.vue', () => {
    let vuetify;
    let wrapper;

    beforeEach(() => {
        vuetify = new Vuetify();
        wrapper = mount(AppRubleInput, {
            vuetify,
            propsData: {
                value: '123.45',
                label: 'Test Label',
                loading: false
            }
        });
    });

    it('renders the component with correct props', () => {
        const input = wrapper.find('input');
        expect(input.element.value).toBe('123.45');
        expect(wrapper.props().label).toBe('Test Label');
        expect(wrapper.props().loading).toBe(false);
    });

    it('emits input event with correct value', async () => {
        const input = wrapper.find('input');
        await input.setValue('678.90');
        expect(wrapper.emitted().input).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual(['678.90']);
    });

    it('emits null when input is empty and nullable is true', async () => {
        await wrapper.setProps({nullable: true});
        const input = wrapper.find('input');
        await input.setValue('');
        expect(wrapper.emitted().input).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual([null]);
    });

    it('applies correct validation rules', async () => {
        const input = wrapper.find('input');
        await input.setValue('invalid value');
        const rules = wrapper.vm.rubleRules;
        expect(rules[0]('')).toBe(true);
        expect(rules[1]('invalid value')).toBe('Положительное число, макс. 2 знака после запятой');
    });
});